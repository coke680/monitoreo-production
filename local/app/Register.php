<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $fillable = [
        'operator','alpha','observations','client_id','zone_id', 'contact', 'aditional_comment',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    // public static $rules =array(
    //         'name' => 'required|min:5',
    //         'email' => 'required|email',
    //         'password' => 'required'
    //     );
}
