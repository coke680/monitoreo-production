<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
	protected $table = 'client_zone';

    protected $fillable = ['client_id', 'zone_name','zone_number',];
}
