<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Register;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
 
 
class ExcelController extends Controller {
 
 /**
 * Display a listing of the resource.
 *
 * @return Response
 */
 public function index()
 {
 
        Excel::create('Registro de eventos', function($excel) {
 
            $excel->sheet('Registros', function($sheet) {
 
                $registers = Register::select('registers.created_at as Fecha', 'users.name as Operador',
                                'clients.client_number as Cliente', DB::raw("CONCAT(clients.name,' ',clients.last_name)  as Nombre_Cliente"), 
                                'client_zone.zone_number as Numero_Zona', 'client_zone.zone_name as Nombre_Zona', 'registers.contact as Contacto', 
                                'alpha as Alpha',  'observations as Observaciones', 'aditional_comment as Comentario_adicional')
                    ->join('users', 'users.id', '=', 'registers.operator')
                    ->join('clients', 'clients.id', '=', 'registers.client_id')
                    ->join('client_zone', 'client_zone.id', '=', 'registers.zone_id');

                    if(request()->has('monthly')){
			            $date = explode('-', request()->monthly);
			            $registers = $registers->whereRaw('YEAR(registers.created_at) = '.$date[0].'')
			                        ->whereRaw('MONTH(registers.created_at) = '.$date[1].'')->orderBy('Fecha')->get();
			        } else {
			            $registers = $registers->whereRaw('MONTH(registers.created_at) = MONTH(CURDATE())')->orderBy('Fecha')->get();
			        }
 
                $sheet->fromArray($registers);
 
            });
        })->export('xls');
 
 }
}