<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Observation;
use Session;

class ObservationsController extends Controller
{
    public function index() {

    	$observations = DB::table('observations')
            ->select('id as id_observation','name as name_observations')
            ->get();

    	return view('observations.index', ['observations' => $observations]);

    }

    public function create() {

		return view('observations.create');
    }

    public function store(Request $request) {	

        $this->validate($request, [
            'observation_name' => 'required|max:500|min:4',
        ]);


    	$observation = New Observation;

    	$observation->name = request('observation_name');

    	$observation->save();
        Session::flash('success', 'Observación guardada exitosamente');
        return redirect()->to('observations');
    }

    public function edit($id) {

        $observation = Observation::find($id);
        return view('observations.edit', compact('observation'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'name' => 'required|max:500|min:4',
        ]);

        $observation = Observation::find($id);

        $observation->update(request()->all());
        Session::flash('success', 'Observación editada exitosamente');
        return redirect()->to('observations');
    }

    public function destroy($id) {
        try {
            $observation = Observation::find($id);

           if ($observation != null){ 
            
                $observation->delete(); 
                Session::flash('warning', 'Observación eliminada exitosamente');
                return back();
            }
            
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Observación no eliminada');
            return back();
        }
    }
}
