<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Zone;
use Session;

class ZoneController extends Controller
{

    // public function __construct(){
        
    //     $this->middleware('auth');
    // }


    public function index() {

    $zones = DB::table('client_zone')
            ->join('clients', 'client_zone.client_id', '=', 'clients.id')
            ->select('client_zone.id as id_zone','client_zone.zone_number as number',
                    'client_zone.zone_name as name','clients.name as client_name', 
                    'clients.last_name as client_last_name','clients.id')
            ->get();


        //$zones = DB::select('SELECT z.id as id_zone, z.zone_number as number, z.zone_name as name, z.client_id, 
                                //c.name as client_name, c.id FROM client_zone z, clients c WHERE z.client_id = c.id;');

    	return view('zones.index', ['zones' => $zones]);
    }

    public function create() {

		return view('zones.create');
    }

    public function store(Request $request) {	

        $this->validate($request, [
            'zone_number' => 'required|integer|min:1',
            'zone_name' => 'required|max:200|min:4',
            'client_id' => 'required|min:1|integer',
        ]);


    	$zone = New Zone;

    	$zone->zone_number = request('zone_number');
    	$zone->zone_name = request('zone_name');
    	$zone->client_id = request('client_id');

    	$zone->save();
        Session::flash('success', 'Zona guardada exitosamente');
        return redirect()->to('zones/create');
    }

    public function edit($id) {

        $zone = Zone::find($id);
        //return view('zones.edit', compact('zones', 'zonee'));
        return view('zones.edit', compact('zone'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'zone_number' => 'required|integer|min:1',
            'zone_name' => 'required|max:200|min:4',
            'client_id' => 'required|integer',
        ]);

        $zone = Zone::find($id);

        $zone->update(request()->all());
        Session::flash('success', 'Zona editada exitosamente');
        return redirect()->to('zones');
    }


    public function destroy($id) {
        try {
            $zone = Zone::find($id);

           if ($zone != null){ 
            
                $zone->delete(); 
                Session::flash('warning', 'Zona eliminada exitosamente');
                return back();
            }
            
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Zona no eliminada');
            return back();
        }
    }
    
}
