<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Client;
use Session;

class ClientsController extends Controller {    

	// public function __construct(){
		
 //        $this->middleware('auth');
 //    }

    public function index() {

        $clientes = DB::table('clients')->select('id', 'client_number as number',
                    'name', 'last_name','address','city_address', 'contact')
                    ->get();

        return view('clients.index', ['clientes' => $clientes]);
    }
    
    public function create() {

    	return view('clients.create');
    }

    public function store(Request $request) {

        $this->validate($request, [
            'client_number' => 'required|integer|min:4|unique:clients',
            'name' => 'required|max:200|min:4',
            'last_name' => 'required|max:200|min:4',
            'address' => 'required|max:200|min:4',
            'city_address' => 'required|max:200|min:4',
            'contact' => 'required|max:200|min:6',
       
        ]);

        Client::create([
            'client_number' => $request['client_number'],
            'name' => $request['name'],
            'last_name' => $request['last_name'],
            'address' => $request['address'],
            'city_address' => $request['city_address'],
            'contact' => $request['contact'],
        ]);

        Session::flash('success', 'Cliente creado exitosamente');
        return redirect()->to('clients');
    }


    public function edit($id){

        $client = Client::find($id);
        //return view('clients.edit', compact('clients', 'cliente'));
        return view('clients.edit', compact('client'));
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'client_number' => 'required|integer|min:4',
            'name' => 'required|max:200|min:4',
            'last_name' => 'required|max:200|min:4',
            'address' => 'required|max:200|min:4',
            'city_address' => 'required|max:200|min:4',
            'contact' => 'required|max:200|min:6',
        ]);

        $client = Client::find($id);

        $client->update(request()->all());
        Session::flash('success', 'Cliente editado exitosamente');
        return redirect()->to('clients');
    }


    public function destroy($id) {
        try {
            $client = Client::find($id);

            $client->delete();
            Session::flash('warning', 'Cliente eliminado exitosamente');
            return back();
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Cliente asociado a algún registro');
            return back();
        }
    }

    	
}
