<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Register;
use App\Zone;
use App\Client;
use Auth;
use Session;
use Jenssegers\Date\Date;
use Yajra\Datatables\Datatables;
Date::setLocale('ES');

class RegistersController extends Controller {   
    
    public function index(Request $request) {
                //Consulta antes del where
        // $query = DB::select('select r.id, r.created_at, u.name as username, r.work_shift, c.client_number,
        //                         c.name as client_name, c.last_name, cz.zone_number, cz.zone_name, r.contact, r.alpha, 
        //                         r.observations from registers r, users u, clients c, client_zone cz'); //Tu consulta sin el where y sin get

        if($request->has('q')) {

            $registers = DB::select('select r.id, r.created_at, u.name as username, r.work_shift, c.client_number,
                                c.name as client_name, c.last_name, cz.zone_number, cz.zone_name, r.contact, r.alpha, 
                                r.observations from registers r, users u, clients c, client_zone cz 
                                where r.operator = u.id and r.client_id = c.id and r.zone_id = cz.id
                                and Date(r.created_at) LIKE \'%'.$request->q.'%\' ORDER BY created_at ASC'); //Tu where con el valor del input
        
        $day = Date::createFromFormat('Y-m-d', $request->q)->format('l j\\,  F');

        }

        else {

            $registers = DB::select('select r.id, r.created_at, u.name as username, r.work_shift, c.client_number,
                                c.name as client_name, c.last_name, cz.zone_number, cz.zone_name, r.contact, r.alpha, 
                                r.observations from registers r, users u, clients c, client_zone cz where r.operator = u.id and r.client_id = c.id and r.zone_id = cz.id
                                and Date(r.created_at) = CURDATE() ORDER BY created_at ASC');

        $today = Date::now()->format('l j\\,  F');
        $day = $today;

        }

        return view('registers.index', compact('registers','day'));
        
    }

    
    public function create() {

    	return view('registers.create');
    }

    public function store(Request $request)  {	

        $this->validate($request, [

            'alpha' => 'required|max:200|alpha',
            'client_id' => 'required|min:1',
            'observations' => 'required|max:500|min:1',
            'zone_id' => 'required|min:1',
            'contact' => 'required|max:200|min:1',
            'aditional_comment' => 'max:500|min:1',
        ]);

        $input = $request->all();
        $zones = $input["zone_id"];
        foreach ($zones as $zone) {

            $register = New Register;

            $register->operator = Auth::id();
            $register->alpha = request('alpha');
            $register->client_id = request('client_id');
            $register->observations = request('observations');
            $register->zone_id = $zone;
            $register->contact = request('contact');
            $register->aditional_comment = request('aditional_comment');
            $register->save();
        }

        Session::flash('success', 'Registro guardado exitosamente');
        return redirect()->to('registers/create');
    }

    public function getInfo($id)
    {
        $zona = Zone::select('id','zone_number','zone_name')->whereClientId($id)->get();
        $cliente = Client::select('id','name','last_name','contact')->whereId($id)->get(); 

        return compact('cliente', 'zona');
    }

    public function getDailyRegister($date) {

        $registers = Register::select('*')->whereDate('created_at', '=', $date)->get();
        return $registers;
        
    }

    public function edit($id) {

        $register = Register::find($id);
        $zona = Zone::select('id','zone_number','zone_name')->whereId($register->zone_id)->get();
        return view('registers.edit', compact('register','zona'));
    }

    public function update(Request $request, $id) {

        $this->validate($request, [
            'alpha' => 'required|max:200|alpha',
            'client_id' => 'required|min:1',
            'observations' => 'required|max:500|min:1',
            'zone_id' => 'required|min:1',
            'contact' => 'required|max:200|min:1',
            'aditional_comment' => 'max:500|min:1',
        ]);

        $register = Register::find($id);

        $register->update(request()->all());

        Session::flash('warning', 'Registro editado exitosamente');
        return redirect()->to('registers');
    }

    public function destroy($id) {
        try {
            $register = Register::find($id);

            $register->delete();
            Session::flash('warning', 'Registro eliminado');
            return back();
        }
        catch(\Illuminate\Database\QueryException $e) {
            Session::flash('error', 'Error. Registro no pudo ser eliminado');
            return back();
        }
    }

    public function monthly(Request $request) {
                //Consulta antes del where
        // $query = DB::select('select r.id, r.created_at, u.name as username, r.work_shift, c.client_number,
        //                         c.name as client_name, c.last_name, cz.zone_number, cz.zone_name, r.contact, r.alpha, 
        //                         r.observations from registers r, users u, clients c, client_zone cz'); //Tu consulta sin el where y sin get

        // if($request->has('monthly')) {

        //     $registers = DB::select('select r.id, r.created_at, u.name as username, r.work_shift, c.client_number,
        //                         c.name as client_name, c.last_name, cz.zone_number, cz.zone_name, r.contact, r.alpha, 
        //                         r.observations from registers r, users u, clients c, client_zone cz 
        //                         where r.operator = u.id and r.client_id = c.id and r.zone_id = cz.id
        //                         and Date(r.created_at) LIKE \'%'.$request->monthly.'%\' ORDER BY created_at ASC'); //Tu where con el valor del input
            
        //     $date = Date::createFromFormat('Y-m', $request->monthly)->format('F');

        // }

        // else {

        //     $registers = DB::select('select r.id, r.created_at, u.name as username, r.work_shift, c.client_number,
        //                         c.name as client_name, c.last_name, cz.zone_number, cz.zone_name, r.contact, r.alpha, 
        //                         r.observations from registers r, users u, clients c, client_zone cz where r.operator = u.id and r.client_id = c.id and r.zone_id = cz.id
        //                         and MONTH(r.created_at) = MONTH(CURDATE()) ORDER BY created_at ASC');
            
        $today = Date::now();
        $date = $today->format('F');

        // }

        return view('registers.monthly', compact('date'));
        
    }

    public function getDataJson(Request $request) {

        $registers = Register::select('registers.id', 'registers.created_at', 'users.name as username', 'work_shift',
                                'clients.client_number as cnumber', DB::raw("CONCAT(clients.name,' ',clients.last_name)  as client_name"), 
                                'client_zone.zone_number as znumber', 'client_zone.zone_name as zname', 'registers.contact', 'alpha',  'observations')
                    ->join('users', 'users.id', '=', 'registers.operator')
                    ->join('clients', 'clients.id', '=', 'registers.client_id')
                    ->join('client_zone', 'client_zone.id', '=', 'registers.zone_id');

        if($request->has('monthly')){
            $date = explode('-', $request->monthly);
            $registers = $registers->whereRaw('YEAR(registers.created_at) = '.$date[0].'')
                        ->whereRaw('MONTH(registers.created_at) = '.$date[1].'');
        } else {
            $registers = $registers->whereRaw('MONTH(registers.created_at) = MONTH(CURDATE())');
        }


        return Datatables::of($registers)
                        ->filterColumn('client_name', function($q, $k){
                            $q->whereRaw('clients.name LIKE  \'%'.$k.'%\' ');
                        })
                        ->filterColumn('username', function($q, $k){
                            $q->whereRaw('users.name LIKE  \'%'.$k.'%\' ');
                        })
                        ->filterColumn('cnumber', function($q, $k){
                            $q->whereRaw('clients.client_number LIKE  \'%'.$k.'%\' ');
                        })
                        ->filterColumn('znumber', function($q, $k){
                            $q->whereRaw('client_zone.zone_number LIKE  \'%'.$k.'%\' ');
                        })
                        ->filterColumn('zname', function($q, $k){
                            $q->whereRaw('client_zone.zone_name LIKE  \'%'.$k.'%\' ');
                        })
                        ->make(true);
    }

}
