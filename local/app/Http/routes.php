<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'auth'], function(){

	Route::get('/', function () {
	    return redirect('registers/create');
	});

	Route::get('/home', 'HomeController@index');
	//registros
	Route::get('/registers', 'RegistersController@index');
	Route::get('/registers/create', 'RegistersController@create');
	Route::post('/registers', 'RegistersController@store');
	Route::get('/registers/monthly', 'RegistersController@monthly');
	
	Route::get('/registers/del/{id}', ['uses' => 'RegistersController@destroy']);
	Route::get('/registers/edit/{id}', ['uses' => 'RegistersController@edit']);
	Route::post('/registers/{id}', ['uses' => 'RegistersController@update']);

	Route::get('/registers/getInfo/{client_id}', 'RegistersController@getInfo');
	Route::get('/registers/monthly', 'RegistersController@monthly');
	// JSON para tabla de registros mensuales
	Route::get('/api/getDataJson', 'RegistersController@getDataJson');
	//Excel
	Route::get('/registers/monthly/excel', 'ExcelController@index');


	//clientes
	Route::get('/clients', 'ClientsController@index');
	Route::get('/clients/create', 'ClientsController@create');
	Route::post('/clients', 'ClientsController@store');
	Route::get('/clients/del/{id}', ['uses' => 'ClientsController@destroy']);
	Route::get('/clients/edit/{id}', ['uses' => 'ClientsController@edit']);
	Route::post('/clients/{id}', ['uses' => 'ClientsController@update']);

	//usuarios

	Route::group(['middleware' => ['admin']], function () {
    	Route::get('/users', 'UserController@index');
		Route::get('/users/create', 'UserController@create');
		Route::post('/users', 'UserController@store');
		Route::get('/users/del/{id}', ['uses' => 'UserController@destroy']);
		Route::get('/users/edit/{id}', ['uses' => 'UserController@edit']);
		Route::post('/users/{id}', ['uses' => 'UserController@update']);
    });
	
	//Zonas
	Route::get('/zones', 'ZoneController@index');
	Route::get('/zones/create', 'ZoneController@create');
	Route::post('/zones', 'ZoneController@store');
	Route::get('/zones/del/{id}', ['uses' => 'ZoneController@destroy']);
	Route::get('/zones/edit/{id}', ['uses' => 'ZoneController@edit']);
	Route::post('/zones/{id}', ['uses' => 'ZoneController@update']);

	//observaciones
	Route::get('/observations', 'ObservationsController@index');
	Route::get('/observations/create', 'ObservationsController@create');
	Route::post('/observations', 'ObservationsController@store');
	Route::get('/observations/del/{id}', ['uses' => 'ObservationsController@destroy']);
	Route::get('/observations/edit/{id}', ['uses' => 'ObservationsController@edit']);
	Route::post('/observations/{id}', ['uses' => 'ObservationsController@update']);

});

Route::auth();
