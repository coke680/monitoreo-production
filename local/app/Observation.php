<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    protected $table = 'observations';

    protected $fillable = ['id', 'name',];
}
