<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
   	protected $fillable = ['client_number', 'name', 'last_name','address','city_address', 'contact',];
}
