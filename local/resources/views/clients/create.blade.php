@extends ('layouts.app')

@section('content')

	<div class="row">
	<div class="col-lg-6">
		<form action="{{ url('/clients') }}" method="POST" role="form">
			<h3><i class="fa fa-edit"></i> Ingresar clientes</h3><hr>

			{{ csrf_field() }}


			<div class="form-group {{ $errors->has('client_number') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="client_number" value="{{ old('client_number') }}" name="client_number" placeholder="Número de cliente">
                @if ($errors->has('client_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_number') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="name" name="name" value="{{ old('name') }}" placeholder="Nombres">
				@if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="last_name" name="last_name" value="{{ old('last_name') }}" placeholder="Apellidos">
				@if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="address" name="address" value="{{ old('address') }}" placeholder="Dirección">
				@if ($errors->has('address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group {{ $errors->has('city_address') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="city_address" name="city_address" value="{{ old('city_address') }}" placeholder="Ciudad">
				@if ($errors->has('city_address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('city_address') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="contact" name="contact" value="{{ old('contact') }}" placeholder="Contacto">
				@if ($errors->has('contact'))
                    <span class="help-block">
                        <strong>{{ $errors->first('contact') }}</strong>
                    </span>
                @endif
			</div>	

			<button type="submit" class="btn btn-primary">Guardar cliente</button>
		</form>
	</div>

</div>
@endsection