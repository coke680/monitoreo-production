@extends ('layouts.app')

@section('content')

    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif

	<h3>Listado de clientes</h3><hr>
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="clientsTable">
			<thead>
				<tr>
					<th>Número de cliente</th>
					<th>Nombre</th>
                    <th>Dirección</th>
                    <th>Ciudad</th>
					<th>Contacto</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach($clientes as $client)
				<tr>
					<td>{{ $client->number }}</td>
					<td>{{ $client->name }} {{ $client->last_name }}</td>
                    <td>{{ $client->address }}</td>
                    <td>{{ $client->city_address }}</td>
					<td>{{ $client->contact }}</td>
					<td><a href="clients/edit/{{ $client->id }}" class="btn btn-xs btn-warning margin-button">Editar</a><a href="clients/del/{{ $client->id }}" onclick="return confirm('¿Está seguro de eliminar al cliente?')" class="btn btn-xs btn-danger">Eliminar</a></td> 
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endsection

@push('datatable')
  <script>
    $(document).ready(function(){
        $('#clientsTable').DataTable({

            responsive: true,
            processing: true,
            bLengthChange: false,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },
            dom: 'Bfrtip',
            buttons: [
                { extend: 'pdf', className: 'btn btn-default margin-button', exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                } },
                { extend: 'excel', className: 'btn btn-default', exportOptions: {
                    columns: [ 0, 1, 2, 3, 4 ]
                } },

            ],
            columnDefs: [
                { width: 10, targets: 0 },
                { width: 100, targets: 1 },
                { width: 100, targets: 2 },
                { width: 100, targets: 3 },
                { width: 100, targets: 4 },
                { width: 50, targets: 5 },
            ],
            fixedColumns: true,

        });

    });
</script>
@endpush