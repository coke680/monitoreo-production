@extends ('layouts.app')

@section('content')

	   @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    
	<h3>Listado de usuarios</h3><hr>

	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="usersTable">
			<thead>
				<tr>
					<th>Usuario</th>
					<th>Email</th>
                    <th>Perfil</th>
					<th>Opciones</th>

				</tr>
			</thead>
			<tbody>
			@foreach ($users as $user) 
				<tr>					
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
                    <td>{{ $user->level }}</td>
					<td><a href="users/edit/{{ $user->id }}" class="btn btn-xs btn-warning margin-button">Editar</a><a href="users/del/{{ $user->id }}" class="btn btn-xs btn-danger" onclick="return confirm('¿Está seguro de eliminar esta zona?')">Eliminar</a></td>					
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>

@endsection

@push('datatable')
  <script>
    $(document).ready(function(){
        $('#usersTable').DataTable({

            responsive: true,
            processing: true,
            bLengthChange: false,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },
            dom: 'Bfrtip',
            buttons: [
                { extend: 'pdf', className: 'btn btn-default margin-button', exportOptions: {
                    columns: [ 0, 1, 2 ]
                } },
                { extend: 'excel', className: 'btn btn-default', exportOptions: {
                    columns: [ 0, 1, 2 ]
                } },

            ],
            columnDefs: [
                { width: 50, targets: 0 },
                { width: 100, targets: 1 },
                { width: 100, targets: 2 },
                { width: 50, targets: 3 },
            ],
            fixedColumns: true,

        });

    });
</script>
@endpush