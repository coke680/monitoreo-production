@extends ('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-6">
		<form action="{{ url('/zones/'.$zone->id.'') }}" method="POST" role="form">
			<legend><i class="fa fa-edit"></i> Editar "{{ $zone->zone_name }}"</legend>
			{{ csrf_field() }}

			<div class="form-group {{ $errors->has('client_id') ? ' has-error' : '' }}">
				<label class="control-label">Cliente</label>
				<select name="client_id" id="input" class="form-control chosen" required>
					<option value="">- Seleccione cliente -</option>
					@foreach(\App\Client::select('id', 'client_number', 'name', 'last_name')->get() as $client)
						<option value="{{ $client->id }}" @if( $client->id == old('zone', $zone->client_id) ) selected="selected" @endif>{{ $client->client_number }} - {{ $client->name }} {{ $client->last_name }}</option>
					@endforeach
				</select>

				@if ($errors->has('client_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_id') }}</strong>
                    </span>
                @endif

			</div>
			<div class="form-group {{ $errors->has('zone_name') ? ' has-error' : '' }}">
				<label class="control-label">Nombre de zona</label>
				<input type="text" class="form-control" required id="zone_name" name="zone_name" placeholder="Nombre zona" value="{{ old('zone_name', $zone->zone_name)}}">

				@if ($errors->has('zone_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zone_name') }}</strong>
                    </span>
                @endif

			</div>
			<div class="form-group {{ $errors->has('zone_number') ? ' has-error' : '' }}">
				<label class="control-label">Número de zona</label>
				<input type="text" class="form-control" required id="zone_number" name="zone_number" value="{{ old('zone_number', $zone->zone_number)}}" placeholder="Número de zona">

				@if ($errors->has('zone_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zone_number') }}</strong>
                    </span>
                @endif

             </div>

			<button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-edit"></i> Editar zona</button>
			<a href="/zones" class="btn btn-default pull-right"><i class="fa fa-btn fa-close"></i> Cancelar</a>
		</form>
	</div>

</div>
@endsection

@push('script')

<script>
	$(document).ready(function(){
		$(".chosen").chosen();
	});
</script>

@endpush