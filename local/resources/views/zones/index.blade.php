@extends ('layouts.app')

@section('content')

	@if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    
	<h3>Listado de zonas</h3><hr>

	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="zoneTable">
			<thead>
				<tr>
					<th>Número de zona</th>
					<th>Nombre de zona</th>
					<th>Cliente</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($zones as $zone) 
				<tr>					
					<td>{{ $zone->number }}</td>
					<td>{{ $zone->name }} </td>
					<td>{{ $zone->client_name }} {{ $zone->client_last_name }}</td>
					<td><a href="zones/edit/{{ $zone->id_zone }}" class="btn btn-xs btn-warning margin-button">Editar</a><a href="zones/del/{{ $zone->id_zone }}" onclick="return confirm('¿Está seguro de eliminar esta zona?')" class="btn btn-xs btn-danger">Eliminar</a></td> 				
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>

@endsection

@push('datatable')
  <script>
    $(document).ready(function(){
        $('#zoneTable').DataTable({

            responsive: true,
            processing: true,
            bLengthChange: false,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },
            dom: 'Bfrtip',
            buttons: [
                { extend: 'pdf', className: 'btn btn-default margin-button', exportOptions: {
                    columns: [ 0, 1, 2 ]
                } },
                { extend: 'excel', className: 'btn btn-default', exportOptions: {
                    columns: [ 0, 1, 2 ]
                } },

            ],
            columnDefs: [
                { width: 50, targets: 0 },
                { width: 100, targets: 1 },
                { width: 100, targets: 2 },
                { width: 100, targets: 3 }
            ],
            fixedColumns: true,

        });

    });
</script>
@endpush