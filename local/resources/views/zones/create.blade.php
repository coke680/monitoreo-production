@extends ('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-6">
	
		@if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
	    @endif

	    @if (Session::has('warning'))
	        <div class="alert alert-warning alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('warning') }}</strong>
	        </div>
	    @endif

	    @if (Session::has('success'))
	        <div class="alert alert-success alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('success') }}</strong>
	        </div>
	    @endif

		<form action="{{ url('/zones') }}" method="POST" role="form">
			<h3><i class="fa fa-edit"></i> Ingresar zonas</h3><hr>
			{{ csrf_field() }}

			<div class="form-group {{ $errors->has('work_shift') ? ' has-error' : '' }}">
				<select name="client_id" id="input" class="form-control chosen" required>
					<option value="">- Seleccione cliente -</option>
					@foreach(\App\Client::select('id', 'client_number', 'name', 'last_name')->get() as $client)
						<option value="{{ $client->id }}">{{ $client->client_number }} - {{ $client->name }} {{ $client->last_name }}</option>
					@endforeach
				</select>
				@if ($errors->has('client_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_id') }}</strong>
                    </span>
                @endif
			</div>
			<div class="form-group {{ $errors->has('zone_name') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="zone_name" name="zone_name" placeholder="Nombre zona">

				@if ($errors->has('zone_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zone_name') }}</strong>
                    </span>
                @endif

			</div>
			<div class="form-group {{ $errors->has('zone_number') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="zone_number" name="zone_number" placeholder="Número de zona">

				@if ($errors->has('zone_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zone_number') }}</strong>
                    </span>
                @endif

			</div>

			<button type="submit" class="btn btn-primary">Crear zona</button>
		</form>
	</div>

</div>
@endsection

@push('script')

<script>
	$(document).ready(function(){
		$(".chosen").select2({
			"language": {
		       "noResults": function(){
		           return "Resultado no encontrado.";
		       }
		   },
		    escapeMarkup: function (markup) {
		        return markup;
		    }
		});
	});
</script>

@endpush
