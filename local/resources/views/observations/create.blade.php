@extends ('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-6">
	
		@if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
	    @endif

	    @if (Session::has('warning'))
	        <div class="alert alert-warning alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('warning') }}</strong>
	        </div>
	    @endif

	    @if (Session::has('success'))
	        <div class="alert alert-success alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('success') }}</strong>
	        </div>
	    @endif

		<form action="{{ url('/observations') }}" method="POST" role="form">
			<h3><i class="fa fa-edit"></i> Ingresar nueva observación</h3><hr>
			{{ csrf_field() }}


			<div class="form-group {{ $errors->has('observation_name') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required id="observation_name" name="observation_name" placeholder="Ingrese observación">

				@if ($errors->has('observation_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('observation_name') }}</strong>
                    </span>
                @endif

			</div>

			<button type="submit" class="btn btn-primary">Crear observación</button>
		</form>
	</div>

</div>
@endsection

