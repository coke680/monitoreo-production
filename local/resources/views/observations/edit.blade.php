@extends ('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-6">
		<form action="{{ url('/observations/'.$observation->id.'') }}" method="POST" role="form">
			<legend><i class="fa fa-edit"></i> Editar "{{ $observation->name }}"</legend>
			{{ csrf_field() }}

			
			<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
				<label class="control-label">Nombre de la observación</label>
				<input type="text" class="form-control" required id="name" name="name" placeholder="Ingrese observación" value="{{ old('name', $observation->name)}}">

				@if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif

			</div>			

			<button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-edit"></i> Editar observación</button>
			<a href="/observations" class="btn btn-default pull-right"><i class="fa fa-btn fa-close"></i> Cancelar</a>
		</form>
	</div>

</div>
@endsection
