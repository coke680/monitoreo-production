@extends ('layouts.app')

@section('content')

	@if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    
	<h3>Listado de observaciones <a href="observations/create" class="btn btn-success btn-sm pull-right"><i class="fa fa-edit fa-btn"></i> Nueva observación</a></h3><hr>

	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="observationsTable">
			<thead>
				<tr>
					<th>Identificador</th>
					<th>Nombre de observación</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($observations as $observation) 
				<tr>					
					<td>{{ $observation->id_observation }}</td>
					<td>{{ $observation->name_observations }} </td>
					<td><a href="observations/edit/{{ $observation->id_observation }}" class="btn btn-xs btn-warning margin-button">Editar</a><a href="observations/del/{{ $observation->id_observation }}" onclick="return confirm('¿Está seguro de eliminar esta observación?')" class="btn btn-xs btn-danger">Eliminar</a></td> 				
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>

@endsection

@push('datatable')
  <script>
    $(document).ready(function(){
        $('#observationsTable').DataTable({

            responsive: true,
            processing: true,
            bLengthChange: false,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },
            dom: 'Bfrtip',
            buttons: [
                { extend: 'pdf', className: 'btn btn-default margin-button', exportOptions: {
                    columns: [ 0, 1 ]
                } },
                { extend: 'excel', className: 'btn btn-default', exportOptions: {
                    columns: [ 0, 1 ]
                } },

            ],
            columnDefs: [
                { width: 50, targets: 0 },
                { width: 100, targets: 1 },
                { width: 100, targets: 2 }
            ],
            fixedColumns: true,

        });

    });
</script>
@endpush