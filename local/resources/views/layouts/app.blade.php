<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/icon" href="{{ url('/logo.png') }}"/>

    <title>Registro de eventos</title>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="{{ url('local/public/css/custom.css') }}" rel="stylesheet">

        <!-- JavaScripts -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">

@if(Auth::check())
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="" href="{{ url('/registers/create') }}">
                    <img src="{{ url('/logo.png') }}" alt="" class="img-responsive">
                </a>
            </div>  

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Registros <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="{{ url('/registers/create') }}"><i class="fa fa-plus-circle fa-btn"></i> Crear registros</a></li>             
                        <li><a href="{{ url('/observations') }}"><i class="fa fa-eye fa-btn"></i> Observaciones</a></li>                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="{{ url('/registers/') }}"><i class="fa fa-calendar-check-o fa-btn"></i> Reporte diario</a></li>                
                        <li><a href="{{ url('/registers/monthly') }}"><i class="fa fa-calendar fa-btn"></i> Reporte mensual</a></li>                        
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Clientes <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="{{ url('/clients') }}"><i class="fa fa-eye fa-btn"></i> Ver clientes</a></li>                
                        <li><a href="{{ url('/clients/create') }}"><i class="fa fa-plus-circle fa-btn"></i> Crear clientes</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Zonas <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="{{ url('/zones') }}"><i class="fa fa-location-arrow fa-btn"></i> Ver zonas</a></li>                
                        <li><a href="{{ url('/zones/create') }}"><i class="fa fa-plus-circle fa-btn"></i> Crear zona</a></li>
                      </ul>
                    </li>
                    @if (Auth::user()->level == 'admin')
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="{{ url('/users') }}"><i class="fa fa-users fa-btn"></i> Ver usuarios</a></li>                
                            <li><a href="{{ url('/users/create') }}"><i class="fa fa-plus-circle fa-btn"></i> Crear usuarios</a></li>
                          </ul>
                        </li>
                    @endif              
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                    @else
                        <li><a><i class="fa fa-calendar"></i> {{ \Jenssegers\Date\Date::today()->format('l j\\,  F') }}</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               <i class="fa fa-user"></i> {{ Auth::user()->name }}<span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Cerrar sesión</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
@endif

    <div class="container">
        @yield('content')
    </div>

@if(Auth::check())
    <footer class="footer-distributed">
        <div class="text-center">
            <p>Mujica y Docmac &nbsp;&bull;&nbsp; Control de registro de actividades</p>
        </div>
    </footer>
@endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>


    <script>
        $(document).ready(function(){

            var url = window.location;
            //$('ul.nav a[href="'+ url +'"]').parent().addClass('active');
            $('ul.nav a').filter(function() {
                return this.href == url;
            }).parent().addClass('active');


        });
    </script>

    @stack('script')
    @stack('datatable')

</body>
</html>
