@extends ('layouts.app')

@section('content')

<style>
	.ui-datepicker-calendar {
    	display: none; 
    }​
</style>

    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif

    <h3>Registros mensuales</small></h3><hr>

    <div class="container">
        <div class="row">
                <form class="form-inline" role="search" method="POST" id="search-form">
                <div class="input-group input-group-sm add-on col-md-4">
                  <input type="text" class="form-control date-picker" autocomplete="off" placeholder="Filtrar por mes" id="monthly" name="monthly">
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                  </div>
                </div>
            </form>
            <hr>
            <form class="form-inline" role="search" method="POST" id="search-form">
                <div class="input-group input-group-sm add-on col-md-4">
                  <div class="input-group-btn" >
                    <input name="_method" type="hidden" value="PATCH">
                    <a class="btn btn-success" href="monthly/excel" target="_blank" id="btn-excel"><i class="fa fa-file-excel-o fa-btn"></i> Exportar Excel</a>
                  </div>
                </div>
            </form>
        </div>            
    </div>


    </form><hr>

	<div class="table-responsive">
		<table id="registersTable" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Fecha y hora</th>
					<th>Operador</th>
					<th>Cliente</th>
					<th>Nombre cliente</th>
					<th>Zona</th>
					<th>Nombre zona</th>
					<th>Contacto</th>
					<th>Alpha</th>
					<th>Observaciones</th>
					<th>Opciones</th>
				</tr>
			</thead>
            <tbody></tbody>
		</table>
	</div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Borrar registros</h4>
          </div>
          <div class="modal-body">
            <p>¿Está seguro que desea eliminar el registro?</p>
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-danger" id="btn-confirm" data-method="delete"><i class="fa fa-check" aria-hidden="true"></i> Sí, eliminar registro</a>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>

          </div>
        </div>
      </div>
    </div>

@endsection

@push('datatable')
  <script type="text/javascript">

     $(document).ready(function(){

        $('#search-form').on('change', function(event) {
            var monthly = $('#monthly').val();
            var hrefExcel = 'monthly/excel?monthly=' + monthly;
            $('#btn-excel').attr('href', hrefExcel);
        });

        $('#search-form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.datepicker.regional['es'] = {
             closeText: 'Aceptar',
             prevText: '< Ant',
             nextText: 'Sig >',
             currentText: 'Hoy',
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
             dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
             dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
             dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
             weekHeader: 'Sm',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             isRTL: false,
             showMonthAfterYear: false,
             yearSuffix: ''
        };
        
        $.datepicker.setDefaults($.datepicker.regional['es']);

        $("#monthly").datepicker({

            dateFormat: "yy-mm",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,

            onClose: function(dateText, inst) {


                function isDonePressed(){
                    return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                }

                if (isDonePressed()){
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                    
                     $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
                }
            },
            beforeShow : function(input, inst) {

                inst.dpDiv.addClass('month_year_datepicker')

                if ((datestr = $(this).val()).length > 0) {
                    year = datestr.substring(datestr.length-4, datestr.length);
                    month = datestr.substring(0, 2);
                    $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                    $(this).datepicker('setDate', new Date(year, month-1, 1));
                    $(".ui-datepicker-calendar").hide();
                }
            }

        });

        var oTable = $('#registersTable').DataTable({
            
            processing: true,
            serverSide: true,
            searching: true,            

            ajax: {
                url: '{{ url("/api/getDataJson") }}',
                data: function (d) {
                    d.monthly = $('#monthly').val();                    
                    }
            },

            order: [[ 0, "asc" ]],
            columns: [

                { data: 'created_at', name: 'created_at' },
                { data: 'username', name: 'username' },
                { data: 'cnumber', name: 'cnumber' },
                { data: 'client_name', name: 'client_name' },
                { data: 'znumber', name: 'znumber' },
                { data: 'zname', name: 'zname' },
                { data: 'contact', name: 'contact' },
                { data: 'alpha', name: 'alpha' },
                { data: 'observations', name: 'observations' },
                {
                   mRender: function (data, type, register, e) {
                        var $btns = '';                        
                        $btns += '<a href="edit/'+ register.id +'" class="btn btn-warning btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a> ';
                        $btns += '<a class="btn btn-danger btn-xs delete" data-toggle="modal" href="#modal-delete" data-id="'+ register.id +'"><i class="fa fa-trash"></i></a>';

                        return $btns;
                    }
                }               
                              
            ],

            responsive: true,            

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "processing": "Cargando registros...",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },

            columnDefs: [
                { width: 50, targets: 0 },
                { width: 20, targets: 1 },
                { width: 20, targets: 2 },
                { width: 10, targets: 3 },
                { width: 10, targets: 4 },
                { width: 10, targets: 5 },
                { width: 10, targets: 6 },
                { width: 10, targets: 7 },
                { width: 10, targets: 8 },
                { width: 100, targets: 9 }
            ],
            fixedColumns: true,

        });


        $('#modal-delete').on('show.bs.modal', function(e) {
              var $modal = $(this),
              $id = $(e.relatedTarget).data('id');
              $('#btn-confirm').attr('href','del/'+ $id);
        });

    });


</script>
@endpush