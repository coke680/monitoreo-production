@extends ('layouts.app')

@section('content')

<div class="row">
	<div class="col-lg-6">
		<form action="{{ url('/registers/'.$register->id.'') }}" method="post" role="form">

			<legend><i class="fa fa-edit"></i> Editar registros</legend>
			{{ csrf_field() }}

			<div class="form-group {{ $errors->has('client_id') ? ' has-error' : '' }}">
				<select name="client_id" id="client_id" class="form-control chosen">
					<option value="">- Seleccione cliente -</option>
					@foreach(\App\Client::select('id', 'client_number', 'name', 'last_name')->get() as $client)
						<option value="{{ $client->id }}" @if( $client->id == old('register', $register->client_id) ) selected="selected" @endif>{{ $client->client_number }} - {{ $client->name }} {{ $client->last_name }}</option>
					@endforeach	
				</select>

				@if ($errors->has('client_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_id') }}</strong>
                    </span>
                @endif

			</div>
			<div class="form-group {{ $errors->has('zone_id') ? ' has-error' : '' }}">
				<select name="zone_id" id="zone_id" class="form-control" required>
				@foreach($zona as $zona)
					<option value="{{ $register->zone_id }}" @if( $register->zone_id == old('register', $register->zone_id) ) selected="selected" @endif> 
						{{ $zona->zone_number }} - {{ $zona->zone_name }}
					</option>
				@endforeach
				</select>

				@if ($errors->has('zone_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zone_id') }}</strong>
                    </span>
                @endif

			</div>

			<div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
				<input type="text" value="{{ old('contact', $register->contact)}}" class="form-control" required id="contact" name="contact" placeholder="Contacto">

				@if ($errors->has('contact'))
                    <span class="help-block">
                        <strong>{{ $errors->first('contact') }}</strong>
                    </span>
                @endif

			</div>
			<div class="form-group {{ $errors->has('alpha') ? ' has-error' : '' }}">
				<select name="alpha" id="input" class="form-control" required>
					<option value="">- ¿Alpha? -</option>
					<option value="Si">Sí</option>
					<option value="No">No</option>
				</select>

				@if ($errors->has('alpha'))
                    <span class="help-block">
                        <strong>{{ $errors->first('alpha') }}</strong>
                    </span>
                @endif

			</div>

			<div class="form-group {{ $errors->has('observations') ? ' has-error' : '' }}">
				<select name="observations" id="input" class="form-control chosen-observations" required>
					<option>- Seleccione observación -</option>
					@foreach(\App\Observation::select('id', 'name')->orderBy('name')->get() as $observation)
						<option value="{{ $observation->name }}">&nbsp;&bull;&nbsp; {{ $observation->name }}</option>
					@endforeach
				</select>

				@if ($errors->has('observations'))
                    <span class="help-block">
                        <strong>{{ $errors->first('observations') }}</strong>
                    </span>
                @endif

			</div>

			<div class="form-group {{ $errors->has('aditional_comment') ? ' has-error' : '' }}">
				<input type="text" class="form-control" value="{{ old('aditional_comment', $register->aditional_comment)}}" id="aditional_comment" name="aditional_comment" placeholder="Comentario adicional (opcional)">

				@if ($errors->has('aditional_comment'))
                    <span class="help-block">
                        <strong>{{ $errors->first('aditional_comment') }}</strong>
                    </span>
                @endif

			</div>

			<button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-edit"></i> Editar registro</button>
			<a href="registers" class="btn btn-default pull-right"><i class="fa fa-btn fa-close"></i> Cancelar</a>
		</form>
	</div>

</div>

@endsection

@push('script')
	
	<script>

	$(document).on('change', '#client_id', function(event) {
		event.preventDefault();

		var client_id = $(this).val();
		$.ajax({
			url: '/registers/getInfo/' + client_id,
			type: 'get',
			dataType: 'json',
		})

		.done(function(data) {

		  var zona = data.zona;
		  var cliente = data.cliente;

		  var $model_zona = '#zone_id';
		  //var $model_cliente = '#id_cliente';

	      var $options = "";
	      // var $input_cliente = "";
	      var $nombre_cliente = "";
	      $($model_zona).empty();
	      //$($model_cliente).empty();
	      $(".nombre_cliente").empty();
	      $.each(zona, function(key, value) {
				$options += '<option value="'+value.id+'" >'+value.zone_number+' - '+value.zone_name+'</option>';

			});
	      $($model_zona).append($options);

	      $.each(cliente, function(key, value) {
				// $input_cliente += '<input type="text" value="'+value.name+'" readonly name="'+value.id+'" class="form-control del_value" placeholder="Nombre cliente">';
				$nombre_cliente += '<strong>'+value.name+'</strong>';

			});
	      //$($model_cliente).append($input_cliente);
	      $('.nombre_cliente').append($nombre_cliente);

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	});

	$(document).ready(function(){
		$(".chosen").select2({

			"language": {
		       "noResults": function(){
		           return "Resultado no encontrado.";
		       }
		   },
		    escapeMarkup: function (markup) {
		        return markup;

    }});
	});

	</script>

@endpush
