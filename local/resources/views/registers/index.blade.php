@extends ('layouts.app')

@section('content')

    @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('error') }}</strong>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('warning') }}</strong>
        </div>
    @endif

    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif

	<h3>Registros diarios - <small>{{ $day }}</small></h3><hr>

    <form class="form-inline" role="search">
        <div class="input-group input-group-sm add-on col-md-4">
          <input type="text" class="form-control" autocomplete="off" value="{{ Request::get('q', '') }}" placeholder="Filtrar por fecha" id="q" name="q">
          <div class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
    </form>  

    </form><hr>

	<div class="table-responsive">
		<table id="registersTable" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Fecha y hora</th>
					<th>Operador</th>
					<th>Cliente</th>
					<th>Nombre cliente</th>
					<th>Zona</th>
					<th>Nombre zona</th>
					<th>Contacto</th>
					<th>Alpha</th>
					<th>Observaciones</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody id="table-filter"> 
				@foreach ($registers as $register)
				<tr>
					<td>{{ $register->created_at }}</td>
					<td>{{ $register->username }}</td>
					<td>{{ $register->client_number }}</td>
					<td>{{ $register->client_name }} {{ $register->last_name }}</td>
					<td>{{ $register->zone_number }}</td>
					<td>{{ $register->zone_name }}</td>
					<td>{{ $register->contact }}</td>
					<td>{{ $register->alpha }}</td>
					<td>{{ $register->observations }}</td>
					<td>
            <a href="registers/edit/{{ $register->id }}" class="btn btn-xs btn-warning margin-button"><i class="fa fa-edit"></i> Editar</a>

            <a data-toggle="modal" href="#modal-delete" data-id="{{ $register->id }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Borrar registros</h4>
          </div>
          <div class="modal-body">
            <p>¿Está seguro que desea eliminar el registro?</p>
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-danger" id="btn-confirm" data-method="delete"><i class="fa fa-check" aria-hidden="true"></i> Sí, eliminar registro</a>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>

          </div>
        </div>
      </div>
    </div>

@endsection

@push('datatable')
  <script>
    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.datepicker.regional['es'] = {
             closeText: 'Cerrar',
             prevText: '< Ant',
             nextText: 'Sig >',
             currentText: 'Hoy',
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
             dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
             dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
             dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
             weekHeader: 'Sm',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             isRTL: false,
             showMonthAfterYear: false,
             yearSuffix: ''

        };
        
        $.datepicker.setDefaults($.datepicker.regional['es']);

        $("#q").datepicker({
            dateFormat: "yy-mm-dd"
        });

        $('#registersTable').DataTable({

            responsive: true,
            processing: true,

            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "search": "Filtrar:",
                "zeroRecords": "No encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(Filtrando _MAX_ registros)",
                "paginate": {
                    "previous": "Página anterior",
                    "next": "Página siguiente"
                  },
            },
            dom: 'Bfrtip',
            buttons: [
                { extend: 'pdfHtml5', className: 'btn btn-default margin-button', exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7 , 8 ] , title: 'Reporte de registro de eventos'
                } },
                { extend: 'excelHtml5', className: 'btn btn-default', exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7 , 8 ] , title: 'Reporte de registro de eventos'
            
                } },
                
            ],
            columnDefs: [
                { width: 100, targets: 0 },
                { width: 20, targets: 1 },
                { width: 20, targets: 2 },
                { width: 10, targets: 3 },
                { width: 10, targets: 4 },
                { width: 10, targets: 5 },
                { width: 10, targets: 6 },
                { width: 10, targets: 7 },
                { width: 100, targets: 8 },
                { width: 100, targets: 9 },

            ],
            fixedColumns: true,

        });

        $('#modal-delete').on('show.bs.modal', function(e) {
              var $modal = $(this),
              $id = $(e.relatedTarget).data('id');
              $('#btn-confirm').attr('href','registers/del/'+ $id);
        });

    });


</script>
@endpush