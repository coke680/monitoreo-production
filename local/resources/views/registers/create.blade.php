@extends ('layouts.app')

@section('content')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.js"></script>


<div class="row">
	<div class="col-lg-6">
		@if (Session::has('error'))
	        <div class="alert alert-danger alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('error') }}</strong>
	        </div>
	    @endif

	    @if (Session::has('success'))
	        <div class="alert alert-success alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong>{{ Session::get('success') }}</strong>
	        </div>
	    @endif

		<h3><i class="fa fa-edit"></i> Ingresar registros</h3><hr>
		
		<form action="{{ url('/registers') }}" method="POST" role="form">
			{{ csrf_field() }}

			<div class="form-group {{ $errors->has('client_id') ? ' has-error' : '' }}">
				<select name="client_id" id="client_id" class="form-control chosen">
					<option value="">- Seleccione cliente -</option>
					@foreach(\App\Client::select('id', 'client_number', 'name', 'last_name')->get() as $client)
						<option value="{{ $client->id }}">{{ $client->client_number }} - {{ $client->name }} {{ $client->last_name }}</option>
					@endforeach
				</select>

				@if ($errors->has('client_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client_id') }}</strong>
                    </span>
                @endif

			</div>
			<div class="form-group {{ $errors->has('zone_id') ? ' has-error' : '' }}">
				<select multiple="multiple" data-placeholder="- Seleccione zona -"  name="zone_id[]" id="zone_id" class="chosen form-control" required>
				</select>

				@if ($errors->has('zone_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('zone_id') }}</strong>
                    </span>
                @endif

			</div>

			<div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
				<input type="text" class="form-control" required value="{{ old('contact') }}" id="contact" name="contact" placeholder="Contacto">

				@if ($errors->has('contact'))
                    <span class="help-block">
                        <strong>{{ $errors->first('contact') }}</strong>
                    </span>
                @endif

			</div>
			<div class="form-group {{ $errors->has('alpha') ? ' has-error' : '' }}">
				<select name="alpha" id="input" class="form-control" required>
					<option value="">- ¿Alpha? -</option>
					<option value="Si">Sí</option>
					<option value="No">No</option>
				</select>

				@if ($errors->has('alpha'))
                    <span class="help-block">
                        <strong>{{ $errors->first('alpha') }}</strong>
                    </span>
                @endif
			</div>
			
			<div class="form-group {{ $errors->has('observations') ? ' has-error' : '' }}">
				<select name="observations" id="input" class="form-control chosen-observations" required>
					<option>- Seleccione observación -</option>
					@foreach(\App\Observation::select('id', 'name')->orderBy('name')->get() as $observation)
						<option value="{{ $observation->name }}">&nbsp;&bull;&nbsp; {{ $observation->name }}</option>
					@endforeach
				</select>

				@if ($errors->has('observations'))
                    <span class="help-block">
                        <strong>{{ $errors->first('observations') }}</strong>
                    </span>
                @endif

			</div>

			<div class="form-group {{ $errors->has('aditional_comment') ? ' has-error' : '' }}">
				<textarea type="text" class="form-control" value="{{ old('aditional_comment') }}" id="aditional_comment" name="aditional_comment" placeholder="Comentario adicional (opcional)"></textarea>

				@if ($errors->has('aditional_comment'))
                    <span class="help-block">
                        <strong>{{ $errors->first('aditional_comment') }}</strong>
                    </span>
                @endif

			</div>


			<button type="submit" class="btn btn-primary"><i class="fa fa-btn fa-floppy-o"></i> Guardar registro</button>
		</form>
	</div>

	@include('registers.partial.protocol')

</div>

@endsection

@push('script')
	
	<script>

	$(document).on('change', '#client_id', function(event) {
		event.preventDefault();
		var client_id = $(this).val();
		$.ajax({
			url: 'getInfo/' + client_id,
			type: 'get',
			dataType: 'json',
		})

		.done(function(data) {

		  var zona = data.zona;
		  var cliente = data.cliente;

		  var $model_zona = '#zone_id';
		  //var $model_cliente = '#id_cliente';

	      var $options = "";
	      // var $input_cliente = "";
	      var $nombre_cliente = "";
	      $($model_zona).empty();
	      //$($model_cliente).empty();
	      $(".nombre_cliente").empty();
	      $.each(zona, function(key, value) {
	      	
				$options += '<option value="'+value.id+'">'+value.zone_number+' - '+value.zone_name+'</option>';

			});
	      $($model_zona).append($options);

	      $.each(cliente, function(key, value) {

				$nombre_cliente += '<strong>'+value.name+'</strong>';

			});
	      $('.nombre_cliente').append($nombre_cliente);

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	});

	$(document).ready(function(){
		$(".chosen-observations").select2({

			"language": {
		       "noResults": function(){
		           return "Resultado no encontrado.";
		       }
		   }
		});

		$(".chosen").select2({

			"language": {
		       "noResults": function(){
		           return "Resultado no encontrado.";
		       }
		   },
		    escapeMarkup: function (markup) {
		        return markup;

    }}).change(function(event) {
        if(event.target == this) {
            var value = $(this).val();
            	if (value!==null && value.length > 2) {
            		swal({
						title: '¡POSIBLE ROBO!',
	  					text: 'Se seleccionaron más de 2 zonas. Se debe dar inicio a los protocolos de robo.',  
						confirmButtonText:'<i class="fa fa-thumbs-up"> </i> Entendido',
						type: 'warning'
					});
            	};
        	}
    	});
	});

	</script>

@endpush
