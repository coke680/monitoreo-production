	<div class="col-lg-6">
	    <h3><i class="fa fa-file-text-o"></i> Protocolos a seguir</h3><hr>

		<div role="tabpanel">
		    <!-- Nav tabs -->
		    <ul class="nav nav-tabs" role="tablist">
		    	<li role="presentation"  class="active">
		            <a href="#senal" aria-controls="tab" role="tab" data-toggle="tab"><i class="fa fa-btn fa-signal"></i> Alarma</a>
		        </li>
		        <li role="presentation">
		            <a href="#clientes" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-btn fa-users"></i> Clientes</a>
		        </li>
		        <li role="presentation">
		            <a href="#cctv" aria-controls="tab" role="tab" data-toggle="tab"><i class="fa fa-btn fa-tv"></i> CCTV</a>
		        </li>
		        <li role="presentation">
		            <a href="#claveazul" aria-controls="tab" role="tab" data-toggle="tab"><i class="fa fa-btn fa-exclamation"></i> Clave Azul</a>
		        </li>
		    </ul>

		    <!-- Tab panes -->
		    <div class="tab-content">
		    	<div role="tabpanel" class="tab-pane active" id="senal">
		            <div class="margin-t-5">
		                <h4 class="">SALUDO</h4>
		                <p>Buenos Días/Tarde/Noche, mi nombre es <strong>{{ Auth::user()->name }}</strong>, lo llamo desde la central de monitoreo de Mujica y Docmac Seguridad Integral. ¿Con quién hablo? (una vez obtenido el nombre de la persona solicitar clave)  ¿Podría indicarnos su clave personal? (una vez recepcionada la clave correcta).</p>

		            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		                <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThree">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#one" aria-expanded="false" aria-controls="one">
						          <strong><i class="fa fa-bell fa-btn"></i> En caso de ser una activación de sensor:</strong> 
						        </a>
						      </h4>
						    </div>
						    <div id="one" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						      <div class="panel-body">
						        <p>Hemos recibido una señal de activación en el lugar monitoreado. ¿Usted se encuentra en el lugar? (de ser así) ¿Se encuentra todo en orden en el lugar, es posible que se haya activado por error la alarma? (de ser así, de por terminada la llamada con la despedida). En caso de que no se encuentre en el hogar, solicite la asistencia del cliente en el lugar para poder acceder al lugar en conjunto con el personal del vehículo de verificación.</p> 
		                		<p>La llamada a Carabineros, debe ser registrando el nombre del monitor que está tratando la señal y entregando la información respectiva del tipo de señal recibida, los datos del cliente, la dirección, número de teléfono y nombre de la persona de contacto.</p>
		                		<p>Una activación cruzada genera notificación inmediata a Carabineros</p>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwo">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#two" aria-expanded="false" aria-controls="two">
						          <strong><i class="fa fa-exclamation-circle fa-btn"></i> En caso de ser señal de pánico:</strong>
						        </a>
						      </h4>
						    </div>
						    <div id="two" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						      <div class="panel-body">
						        	<p>Hemos recibido una señal de pánico desde el lugar monitoreado, en estos momentos el vehículo de verificación ya se encuentra en camino y de ser necesario, se procederá a informar a Carabineros, ¿Es posible que pueda acercarse al lugar?</p>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThree">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#three" aria-expanded="false" aria-controls="three">
						          <strong><i class="fa fa-ambulance fa-btn"></i> En caso de ser una señal de emergencia médica:</strong> 
						        </a>
						      </h4>
						    </div>
						    <div id="three" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						      <div class="panel-body">
						        <p>Hemos recibido una señal de emergencia médica desde el lugar monitoreado. (En caso de que quién conteste no sea el cliente) ¿Es posible que pueda acercarse al lugar para verificar que sucede? De todos modos el vehículo de verificación ya se dirige al lugar. (En caso de que conteste el cliente) ¿Podría indicarnos cuál es la situación que ocurre en el lugar?________ (intente hablar con calma y no altere cliente). El vehículo se dirige en este momento al lugar, en caso de ser necesario se procederá a llamar al servicio de salud. Lo volveremos a llamar para rastrear la situación.</p>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThree">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#four" aria-expanded="false" aria-controls="four">
						          <i class="fa fa-fire fa-btn"></i><strong> En caso de ser emergencia con presencia de fuego:</strong><br><span class="text-danger">Se procede primeramente llamar al cliente o a las personas del listado de prioridad de llamada:</span>
						        </a>
						      </h4>
						    </div>
						    <div id="four" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						      <div class="panel-body">
						         <p>Hemos recibido una señal de activación en el lugar monitoreado de presencia de fuego. ¿Usted se encuentra en el lugar? (de ser así) Indíquele al cliente o persona del listado que se dará aviso a una unidad de bomberos de manera inmediata y que dentro de lo posible, se enviará también el vehículo de verificación para ayudar. (De no ser así), ¿Es posible que pueda acercarse al lugar, para tener mejor acceso al lugar? en estos momentos se dirige el vehículo de verificación para analizar la situación en el lugar y de ser necesario será notificado a la unidad de bomberos para acercarse al lugar.</p>
						      </div>
						    </div>
						  </div>

						</div>
		                <hr>

		                <h4 class="">DESPEDIDA</h4>
		                <p>Estimado/a: hemos dado por finalizado el trato de la señal, recuerde que nuestro interés es velar por su seguridad. Estaremos rastreando su señal en caso de una nueva activación. Buen día/tarde/noche.</p>
		            </div>            
		        </div>
		        <div role="tabpanel" class="tab-pane" id="clientes">
		            <div class="margin-t-5">
		                <h4 class="">SALUDO</h4>
		                <p>Buenos días, usted se ha comunicado con la Central de monitoreo de Mujica y Docmac Seguridad Integral. Habla con <strong>{{ Auth::user()->name }}</strong>, Operador/Encargado de la central. ¿En qué puedo ayudarlo?</p><hr>

		                <h4 class="">DESPEDIDA</h4>
		                <p>Muy bien Sr. (ita) <span class="nombre_cliente"></span>, recuerde que nuestro interés es velar por su seguridad. ¿Hay algo más en lo que pueda ayudarla?</p>
		                <p>Buenos días/tardes/noche</p>

		            </div>
		        </div>
		        <div role="tabpanel" class="tab-pane" id="cctv">
		            <div class="margin-t-5">
		                <h4 class="">SALUDO AL CLIENTE O PERSONAL AUTORIZADO.</h4>
		                <p>Buenos Días/Tarde/Noche, mi nombre es <strong>{{ Auth::user()->name }}</strong>, lo llamo desde la central de monitoreo de Mujica y Docmac Seguridad Integral, hemos recibido una señal del tipo _____________.</p><hr>

		            <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
		                <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThree">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#activateonecctv" aria-expanded="false" aria-controls="activateonecctv">
						          <strong><i class="fa fa-bell fa-btn"></i> ACTIVACIÓN DEL SERVICIO (MONITOREO ACTIVO):</strong> 
						        </a>
						      </h4>
						    </div>
						    <div id="activateonecctv" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						      <div class="panel-body">
						        <p>Buenos Días/Tarde/Noche, mi nombre es <strong>{{ Auth::user()->name }}</strong>, favor puede identificarse indicando el nombre del cliente (como empresa), Ubicación Monitoreada, (nombre y Dirección), su nombre completo y clave de seguridad. ¿Se encuentra la ubicación en condiciones de ser monitoreada?</p>

								<p>Si la información no es correcta se debe solicitar pregunta de seguridad, si nuevamente los datos no coinciden, se notificará al Contacto 1 “Encargado de cliente” del Formulario de Contacto por correo electrónico de este error de identificación esperando confirmación para iniciar Monitoreo Activo (espera de 10 minutos), estando durante ese período en Estado de Alerta1, luego si no existe confirmación anterior se inicia Monitoreo Activo, en caso contrario, habilita de inmediato el Servicio.</p>

						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwo">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#activatetwocctv" aria-expanded="false" aria-controls="activatetwocctv">
						          <strong><i class="fa fa-exclamation-circle fa-btn"></i> INGRESO DE PERSONAL AUTORIZADO:</strong>
						        </a>
						      </h4>
						    </div>
						    <div id="activatetwocctv" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						      <div class="panel-body">
						        	<p>Mientras el servicio se mantenga en Monitoreo Activo, el ingreso de personal autorizado, es decir, registrado en Formulario de Contacto, debe ser con el siguiente procedimiento de comunicación con la Central de Monitoreo de Alerta MD</p>

									<p>(Operador recibe llamado)</p>

									<p>Buenos Días/Tarde/Noche, mi nombre es (usuario), favor puede identificarse indicando el nombre del cliente (como empresa), Ubicación Monitoreada, (nombre y Dirección), su nombre completo y clave de seguridad. ¿Hará ya ingreso a la instalación?</p>

									<p>El operario de monitoreo validará esta información y en caso de ser errónea, solicitará la respuesta a la pregunta de seguridad, si nuevamente los datos no coinciden, se notificará al Contacto 1 “Encargado de Cliente” del Formulario de Contacto por correo electrónico de este error de identificación esperando confirmación para autorizar ingreso, manteniendo durante este período el Monitoreo Activo. En caso de que la respuesta a la pregunta sí corresponda o el “Encargado de Cliente” confirme y valide la solicitud de acceso a la ubicación monitoreada, se autoriza el ingreso.</p>
						      </div>
						    </div>
						  </div>

						</div>

		            </div>
           
		        </div>
		        <div role="tabpanel" class="tab-pane" id="claveazul">
		            <div class="margin-t-5">
		                <h4 class="">SALUDO AL CLIENTE O PERSONAL AUTORIZADO.</h4>
		                <p>Buenos Días/Tarde/Noche, mi nombre es <strong>{{ Auth::user()->name }}</strong>, lo llamo desde la central de monitoreo de Mujica y Docmac Seguridad Integral, hemos recibido una señal del tipo _____________.</p>

		                <p>¿Se encuentra con ____________________en el sistema, ha presentado algún problema eléctrico u otro tipo en el lugar? (en caso de señal de falla de batería o falta de testeo)</p>

		                <p>Para poder ayudarla, necesito saber si hay alguien en el lugar donde está la alarma, de ser así, le agradecería en caso de salir del lugar, si aún no ha vuelto la electricidad, llamar para activar clave azul (baja 220).</p>
		                <p>¿Aún no ha cerrado en el lugar, ha tenido algún problema o ha olvidado cerrar? (en caso de señal de señal sin cierre aún).</p><hr>

		            <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
		                <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingThree">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#activateone" aria-expanded="false" aria-controls="activateone">
						          <strong><i class="fa fa-bell fa-btn"></i> INICIO Y CONFIRMACIÓN DE CLAVE AZUL:</strong> 
						        </a>
						      </h4>
						    </div>
						    <div id="activateone" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						      <div class="panel-body">
						        <p>Favor, me puede indicar el período en que se encontrará fuera de su hogar (inicio y término). Durante ese período, ¿Quién quedará como contacto durante este período? (si es contacto de Bykom se valida // sino se le solicita nombre y teléfono de contacto para registrar). ¿Alguien hará ingreso durante este período? (si / no, quien?)</p>

								<p>Muchas gracias por esta información, le comentó que nuestro móvil de reacción irá a visitar el lugar con rondas diurnas y nocturna.</p>

								<p>Nota: ante cualquier activación avisar a contacto, y proceder a notificar a Carabineros.</p>

								<p>Buenos días/tardes/noche</p>
						      </div>
						    </div>
						  </div>
						  <div class="panel panel-default">
						    <div class="panel-heading" role="tab" id="headingTwo">
						      <h4 class="panel-title">
						        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#activatetwo" aria-expanded="false" aria-controls="activatetwo">
						          <strong><i class="fa fa-exclamation-circle fa-btn"></i> FINALIZACIÓN DE CLAVE AZUL:</strong>
						        </a>
						      </h4>
						    </div>
						    <div id="activatetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						      <div class="panel-body">
						        	<p>Confirmo por sistema que se ha registrado una apertura, me valida que ya regresó a su hogar para eliminar la clave azul (si). Muchas gracias. (no) Procederá a verificar con móvil de reacción, le comunicaré a persona de contacto y a Carabineros. Lo mantendremos informado.</p>
						        	<p>Buenos días/tardes/noche</p>
						      </div>
						    </div>
						  </div>

						</div> 
		            </div>
           
		        </div>

		    </div>
		</div>
	</div>