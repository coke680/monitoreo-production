@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="transform: translateY(50%)">
                <div class="panel-heading">Sistema de registros</div>

                <div class="panel-body">

	                <div class="col-lg-4">
						<img src="{{ url('/logo-login.png') }}" class="img-responsive" alt="">
					</div>
					<div class="col-lg-8 margin-top-login">
						<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            

                            <div class="col-md-8 col-md-offset-2">
                                <input id="email" placeholder="Email" type="email" class="form-control" name="email" required="required" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            

                            <div class="col-md-8 col-md-offset-2">
                                <input id="password"placeholder="Password"  type="password" class="form-control" name="password" required="required">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Recordar?
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Iniciar sesión
                                </button>

                            </div>
                        </div>
                    </form>
					</div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
